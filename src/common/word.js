export const thVowels = ['ิ', 'ี', 'ึ', 'ื', 'ุ', 'ู', 'ำ', 'ั'];
export const thSideVowels = ['ะ', 'า', 'เ', 'แ', 'โ', 'ใ', 'ไ'];
export const thToneMarks = ['่', '้', '๊', '๋', '์', '็'];

export const clean = word => {
  let mapping = [];
  for (const [index, val] of word.split('').entries()) {
    if (mapping.length > 0) {
      if (
        thToneMarks.includes(val) &&
        (thToneMarks.includes(mapping[index - 1]) ||
          thSideVowels.includes(mapping[index - 1]))
      ) {
        continue;
      }

      if (
        thVowels.includes(val) &&
        (thToneMarks.includes(mapping[index - 1]) ||
          thVowels.includes(mapping[index - 1]) ||
          thSideVowels.includes(mapping[index - 1]))
      ) {
        continue;
      }
    }

    mapping.push(val);
  }

  return mapping.join('');
};

export const difference = (word, compareWord) => {
  let mapping = [];
  let cWord = clean(word).split('');
  let cCompareWord = compareWord.split('');

  mapping = Array(cCompareWord.length).fill(null);
  for (const [index, val] of cCompareWord.entries()) {
    if (val === cWord[index]) mapping[index] = { value: val, diff: false };
    else {
      if (thToneMarks.includes(val) || thVowels.includes(val)) {
        if (thVowels.includes(mapping[index - 1].value)) {
          mapping[index - 2].diff = true;
        }
        mapping[index - 1].diff = true;
      }

      if (cWord.length < cCompareWord.length) {
        if (cCompareWord[index + 1] === cWord[index + 1]) {
          cWord[index] = val;
        } else {
          cWord.splice(index, 0, val);
        }
      } else if (cWord.length > cCompareWord.length) {
        if (cCompareWord[index] === cWord[index + 1]) {
          cWord.splice(index, 1, val);
        } else {
          cWord[index] = val;
        }
      } else {
        cWord[index] = val;
      }

      mapping[index] = { value: val, diff: true };
    }
  }

  return mapping;
};