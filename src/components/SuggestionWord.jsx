import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { difference } from "../common/word";

const SuggestionWord = ({ word, suggestWord }) => {
  const transfromWord = () => {
    return difference(word, suggestWord).map((value, index) => {
      if (value.diff) {
        return (
          <span key={`c-${index}`} className="text-danger">
            {value.value}
          </span>
        );
      }

      return <span key={`c-${index}`}>{value.value}</span>;
    });
  };

  return <b className="text-primary">{transfromWord()}</b>;
};

SuggestionWord.propTypes = {
  word: PropTypes.string,
  suggestWord: PropTypes.string
};

SuggestionWord.defaultProps = {
  word: "",
  suggestWord: ""
};

export default SuggestionWord;
