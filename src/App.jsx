import React, { useState } from "react";
import styled from "styled-components";
import SuggestionWord from "./components/SuggestionWord";

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 2rem;
`;

export default function App() {
  const [word, setWord] = useState("");
  const [suggestWord, setSuggestWord] = useState("");

  return (
    <div className="App">
      <Wrapper>
        <div className="card shadow w-75">
          <div className="card-body p-4">
            <h3 className="mb-4">ไฮไลท์การสะกดคำผิด</h3>

            <section>
              <div className="form-group mb-2">
                <label>คำที่ทดสอบ</label>
                <input
                  className="form-control"
                  placeholder="ป้อนคำที่ทดสอบ"
                  value={word}
                  onChange={(event) => setWord(event.target.value.trim())}
                />
              </div>
              <div className="form-group mb-2">
                <label>คำที่ถูกต้อง</label>
                <input
                  className="form-control"
                  placeholder="ป้อนคำที่ถูกต้อง"
                  value={suggestWord}
                  onChange={(event) =>
                    setSuggestWord(event.target.value.trim())
                  }
                />
              </div>
              <div className="alert alert-info mt-4 text-center">
                {word && suggestWord ? (
                  <SuggestionWord word={word} suggestWord={suggestWord} />
                ) : (
                  <span>
                    โปรดระบุ <b>คำที่ทดสอบ</b> และ <b>คำที่ถูกต้อง</b>
                  </span>
                )}
              </div>
            </section>
          </div>
        </div>
      </Wrapper>
    </div>
  );
}
